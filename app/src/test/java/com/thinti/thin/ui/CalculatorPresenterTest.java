package com.thinti.thin.ui;

import android.content.Context;
import android.text.TextUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import vn.tlv.caculator.R;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by thuanlv on 08/09/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(TextUtils.class)
public class CalculatorPresenterTest {


    @Mock
    private CalculatorConstant.View mView;

    private CalculatorPresenter mPresenter;

    @Mock
    private Context mContext;

    @Before
    public void setupAddNotePresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mPresenter = new CalculatorPresenter(mView);

        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(any(CharSequence.class))).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                CharSequence a = (CharSequence) invocation.getArguments()[0];
                return !(a != null && a.length() > 0);
            }
        });

        given(mView.context()).willReturn(mContext);
        when(mContext.getString(R.string.error)).thenReturn("error");
    }

    @Test
    public void testClearTextAndResultText() throws Exception {
        mPresenter.clearTextAndResultText();

        verify(mView).clearText();
        verify(mView).clearResultText();
    }

    @Test
    public void testNumberButtonClick() throws Exception {
        mPresenter.numberButtonClick(R.id.zero);
        verify(mView).addLastText("0");

        mPresenter.numberButtonClick(R.id.one);
        verify(mView).addLastText("1");

        mPresenter.numberButtonClick(R.id.two);
        verify(mView).addLastText("2");

        mPresenter.numberButtonClick(R.id.three);
        verify(mView).addLastText("3");

        mPresenter.numberButtonClick(R.id.four);
        verify(mView).addLastText("4");

        mPresenter.numberButtonClick(R.id.five);
        verify(mView).addLastText("5");

        mPresenter.numberButtonClick(R.id.six);
        verify(mView).addLastText("6");

        mPresenter.numberButtonClick(R.id.seven);
        verify(mView).addLastText("7");

        mPresenter.numberButtonClick(R.id.eight);
        verify(mView).addLastText("8");

        mPresenter.numberButtonClick(R.id.nine);
        verify(mView).addLastText("9");
    }

    @Test
    public void testDelButtonClick() throws Exception {
        when(mView.getText()).thenReturn("1+2");
        mPresenter.delButtonClick(mContext);
        verify(mView).setText("1+");

        when(mView.getText()).thenReturn("1+250948309+");
        mPresenter.delButtonClick(mContext);
        verify(mView).setText("1+250948309");

        when(mView.getText()).thenReturn("2345+76-9/");
        mPresenter.delButtonClick(mContext);
        verify(mView).setText("2345+76-9");
    }

    @Test
    public void testDelButtonClickWhenTextEqualError() throws Exception {
        String errorText = mContext.getString(R.string.error);
        when(mView.getText()).thenReturn(errorText);
        mPresenter.delButtonClick(mContext);
        verify(mView).setText("");
    }

    @Test
    public void testOperatorButtonClick() throws Exception {
        when(mView.getText()).thenReturn("111");
        mPresenter.operatorButtonClick(R.id.add);
        verify(mView).setText("111+");

        when(mView.getText()).thenReturn("123/");
        mPresenter.operatorButtonClick(R.id.sub);
        verify(mView).setText("123-");

        when(mView.getText()).thenReturn(null);
        mPresenter.operatorButtonClick(R.id.mul);
        verify(mView).setText("x");

        when(mView.getText()).thenReturn("");
        mPresenter.operatorButtonClick(R.id.div);
        verify(mView).setText("/");
    }

    @Test
    public void testResultButtonClick() throws Exception {
        when(mView.getText()).thenReturn("1+2+3");
        mPresenter.resultButtonClick();
        verify(mView).setText("6.0");
        verify(mView).setResultText("6.0");

        when(mView.getText()).thenReturn("");
        mPresenter.resultButtonClick();
        verify(mView).setText("");
        verify(mView).setResultText("");

        when(mView.getText()).thenReturn(null);
        mPresenter.resultButtonClick();
        verify(mView).setText("");
        verify(mView).setResultText("");

        when(mView.getText()).thenReturn("1+2+3+4");
        mPresenter.resultButtonClick();
        verify(mView).setText("10.0");
        verify(mView).setResultText("10.0");
    }

    @Test
    public void testResultButtonClickWhenTextError() throws Exception {
        when(mView.getText()).thenReturn("12+12+*+2");
        mPresenter.resultButtonClick();
        verify(mView).setTextError();
    }
}