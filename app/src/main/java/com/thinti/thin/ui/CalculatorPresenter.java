package com.thinti.thin.ui;

import android.content.Context;
import android.text.TextUtils;

import com.thinti.thin.R;
import com.thinti.thin.controler.Calculate;

/**
 * Created by thuanlv on 08/09/2016.
 */
public class CalculatorPresenter implements CalculatorConstant.Presenter {

    private CalculatorConstant.View mView;

    public CalculatorPresenter(CalculatorConstant.View view) {
        mView = view;
    }

    @Override
    public void clearTextAndResultText() {
        mView.clearText();
        mView.clearResultText();
    }

    @Override
    public void numberButtonClick(int id) {
        String lastText = "";
        switch (id) {
            case R.id.zero:
                lastText = "0";
                break;
            case R.id.one:
                lastText = "1";
                break;
            case R.id.two:
                lastText = "2";
                break;
            case R.id.three:
                lastText = "3";
                break;
            case R.id.four:
                lastText = "4";
                break;
            case R.id.five:
                lastText = "5";
                break;
            case R.id.six:
                lastText = "6";
                break;
            case R.id.seven:
                lastText = "7";
                break;
            case R.id.eight:
                lastText = "8";
                break;
            case R.id.nine:
                lastText = "9";
                break;
        }
        mView.addLastText(lastText);
    }

    @Override
    public void delButtonClick(Context context) {
        String text = mView.getText();
        if (!TextUtils.isEmpty(text)) {
            if (text.contains(context.getString(R.string.error))) {
                text = "";
            } else {
                text = text.substring(0, text.length() - 1);
            }
        }
        mView.setText(text);
    }

    @Override
    public void operatorButtonClick(int id) {
        String lastText = "";
        switch (id) {
            case R.id.add:
                lastText = "+";
                break;
            case R.id.sub:
                lastText = "-";
                break;
            case R.id.mul:
                lastText = "x";
                break;
            case R.id.div:
                lastText = "/";
                break;
        }
        if (!TextUtils.isEmpty(mView.getText())) {
            String text = mView.getText();
            if (!TextUtils.isEmpty(text)) {
                char last = text.charAt(text.length() - 1);
                if (last == '+' || last == '-' || last == '*' || last == 'x' || last == '/') {
                    text = text.substring(0, text.length() - 1) + lastText;
                } else {
                    text += lastText;
                }
                mView.setText(text);
            }
        } else {
            mView.setText(lastText);
        }
    }

    @Override
    public void resultButtonClick() {
        String text = mView.getText();
        try {
            text = Calculate.calc(text);
            mView.setResultText(text);
            mView.setText(text);
        } catch (Exception e) {
            mView.setTextError();
        }
    }
}
