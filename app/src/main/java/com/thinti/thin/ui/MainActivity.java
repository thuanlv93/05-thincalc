package com.thinti.thin.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.thinti.thin.R;
import com.thinti.thin.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity implements CalculatorConstant.View {
    private ActivityMainBinding mBinding;
    private CalculatorPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

//        Utils.disableSoftInputFromAppearing(mBinding.text);

        mPresenter = new CalculatorPresenter(this);

        mBinding.del.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mPresenter.clearTextAndResultText();
                return false;
            }
        });

        MobileAds.initialize(this, getString(R.string.banner_ad_unit_id));
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mBinding.adView.loadAd(adRequest);
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mBinding.adView != null) {
            mBinding.adView.pause();
        }
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mBinding.adView != null) {
            mBinding.adView.resume();
        }
    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mBinding.adView != null) {
            mBinding.adView.destroy();
        }
        super.onDestroy();
    }

    public void numberButtonClick(View view) {
        mPresenter.numberButtonClick(view.getId());
    }

    public void delButtonClick(View view) {
        mPresenter.delButtonClick(this);
    }

    public void operatorButtonClick(View view) {
        mPresenter.operatorButtonClick(view.getId());
    }

    public void resultButtonClick(View view) {
        mPresenter.resultButtonClick();
    }

    @Override
    public String getText() {
        return mBinding.text.getText().toString();
    }

    @Override
    public void setText(String text) {
        mBinding.text.setText(text);
    }

    @Override
    public void setTextError() {
        mBinding.text.setText(getString(R.string.error));
    }

    @Override
    public void clearText() {
        mBinding.text.setText("");
    }

    @Override
    public void addLastText(String lastText) {
        mBinding.text.setText(mBinding.text.getText().toString() + lastText);
    }

    @Override
    public void clearResultText() {
        mBinding.resultText.setText("");
    }

    @Override
    public void setResultText(String resultText) {
        mBinding.resultText.setText(resultText);
    }

    @Override
    public Context context() {
        return this;
    }
}

