package com.thinti.thin.ui;

import android.content.Context;

/**
 * Created by thuanlv on 08/09/2016.
 */
public class CalculatorConstant {
    interface View {
        String getText();

        void setText(String text);

        void setTextError();

        void clearText();

        void addLastText(String lastText);

        void clearResultText();

        void setResultText(String resultText);

        Context context();
    }

    interface Presenter {
        void clearTextAndResultText();

        void numberButtonClick(int id);

        void delButtonClick(Context context);

        void operatorButtonClick(int id);

        void resultButtonClick();
    }

}