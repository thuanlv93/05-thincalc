package com.thinti.thin.controler;

import android.os.Build;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by thuanlv on 08/09/2016.
 */
public class Utils {
    public static void disableSoftInputFromAppearing(EditText editText) {
        if (Build.VERSION.SDK_INT >= 11) {
            editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
            editText.setTextIsSelectable(true);
        } else {
            editText.setRawInputType(InputType.TYPE_NULL);
            editText.setFocusable(true);
        }
    }
}
