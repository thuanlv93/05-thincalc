package com.thinti.thin.ui;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import vn.tlv.caculator.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by thuanlv on 12/09/2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class CalculatorScreenTest {

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void clickResultButton_showResult() {
        // 1
        onView(withId(R.id.one)).perform(click());

        // 1+
        onView(withId(R.id.add)).perform(click());

        // 1+2
        onView(withId(R.id.two)).perform(click());

        // 1+2-
        onView(withId(R.id.sub)).perform(click());

        // 1+2*
        onView(withId(R.id.mul)).perform(click());

        // 1+2*3
        onView(withId(R.id.three)).perform(click());

        onView(withId(R.id.result)).perform(click());

        onView(withId(R.id.resultText)).check(matches(isDisplayed()));
        onView(withId(R.id.text)).check(matches(isDisplayed()));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}